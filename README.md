# My workstation build

This document will explain how to set up an out of the box Linux/Mac device so that it can function within the DevOps team.

---

## Initial Startup

Follow the on screen instruction when you first power up the device.

---

## Quickstart command

This script has been tested on:

* Ubuntu/PopOS 22.04

* Ubuntu/PopOS 24.04
  
NB. For 24.04, you must first do the following:

```bash
sudo apt update && sudo apt install -y curl
```

* Debian 12.5
  
NB. For Debian 12, you must first do the following:

```bash
su -
```

Then...

```bash
apt update && apt install -y curl
usermod -aG sudo $(logname)
reboot
```

* MacOS Sonoma 14.5

After this you can login as your non-privileged user to continue the installation.

### For bash/zsh shells

```bash
/bin/bash -c "$(curl -fsSL https://gitlab.com/gsl/workstation/-/raw/master/install.sh)"
```

### For fish

```fish
/bin/bash -c "(curl -fsSL https://gitlab.com/gsl/workstation/-/raw/master/install.sh)"
```

## Known Issues

1. Ubuntu 24.04 is not currently supported, the upstream ansible role for
   vscode requires apt packages that are not available for the OS.  Also,
   curl is missing from the default install and must be manually installed
   before running the web-based install script.

2. After running on Linux workstations, the user can no longer reboot or shutdown
the computer, without privilege escalation.  To work around this, use:

```bash
sudo reboot
```

or

```bash
sudo shutdown -h now
```
