#!/bin/bash

grp_dir=~/git/gsl
repo_dir=$grp_dir/workstation

function os_detection() {
  # bash v3 comptibility for macos
  osInfo=("/etc/redhat-release:yum"
    "/etc/arch-release:pacman"
    "/etc/gentoo-release:emerge"
    "/etc/SuSE-release:zypp"
    "/etc/debian_version:sudo apt-get -y"
    "/etc/alpine-release:apk"
    "/etc/zshrc_Apple_Terminal:brew")

  for entry in "${osInfo[@]}"; do
    file="${entry%%:*}"
    if [[ -f "$file" ]]; then
      installer="${entry#*:}"
      echo "Package manager: $installer"
      break
    fi
  done
  if [ -e "$installer" ]; then
    echo "Sorry, package manager not recognised"
    exit 1
  fi
}

function encrypted_disk_check_linux() {
  home_device=$(df /home | tail -1 | awk '{print $1}')
  home_device_short=$(echo "$home_device" | awk -F'/' '{print $NF}')
  
  # Get the full tree of the device (including the type of each layer)
  lsblk_output=$(lsblk -o NAME,TYPE -n)

  # Check if any part of the device path contains 'crypt'
  if echo "$lsblk_output" | grep -q -w "$home_device_short"; then
    parent_device=$(echo "$lsblk_output" | awk -v hd="$home_device_short" '$1 == hd {print $1}')
    if echo "$lsblk_output" | grep -q -w "$parent_device.*crypt"; then
      echo "The /home directory is on an encrypted disk."
    else
      echo "The /home directory is not on an encrypted disk. Refusing to install."
      exit 6
    fi
  else
    echo "Device for /home not found. Refusing to install."
    exit 6
  fi
}

function check_filevault_enabled() {
  if fdesetup status | grep -q "FileVault is On"; then
    echo "FileVault is enabled."
  else
    echo "FileVault is not enabled. Refusing to install on unencrypted disk."
    exit 7
  fi
}

function install_homebrew() {
  # We only want this on macos

  # Check if Xcode Command Line Tools are already installed
  if xcode-select -p &>/dev/null; then
    echo "Xcode Command Line Tools are already installed."
  else
    # Install Xcode Command Line Tools
    echo "This will install xcode cli tools, please return to the cli once complete..."
    sudo xcode-select --install
    echo "Paused, do not proceed until xcode cli installation has completed.  Press return when done."
    read -r
    sudo xcodebuild -license accept
  fi

  echo "Installing homebrew..."
  echo /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
}

# Call the function
os_detection
echo "installer is now $installer"

case "$installer" in
*"apt-get"*)
  encrypted_disk_check_linux
  echo "If prompted, please enter your password for 'sudo'."
  [[ -r /etc/needrestart/needrestart.conf ]] && sudo sed -i -E 's/#?\$nrconf\{restart\} = .*/$nrconf{restart} = "a";/' /etc/needrestart/needrestart.conf
  # shellcheck disable=SC2174
  [[ -d /var/run/sshd ]] || sudo mkdir -p -m0755 /var/run/sshd
  $installer update
  $installer install python3 python3-pip python3-virtualenv openssh-server
  ;;
brew)
  check_filevault_enabled
  which brew || install_homebrew
  $installer install virtualenv
  ;;
*)
  echo "unknown package manager"
  exit 2
  ;;
esac

echo "Installing git"
$installer install git
echo "Pulling config git repo"
if [ ! -d $repo_dir ]; then
  mkdir -p $grp_dir
  cd $grp_dir || exit 1
  git clone https://gitlab.com/gsl/workstation.git
  cd $repo_dir || exit 2
else
  cd $repo_dir || exit 3
  git pull
fi

echo "Installing venv for ansible"
pip_cmd=$(which pip || which pip3)
virtualenv .venv || exit 4
# shellcheck source=/dev/null
source .venv/bin/activate
cd $repo_dir || exit 5
.venv/bin/"$(basename "$pip_cmd")" install -r requirements.txt
ansible-galaxy install -r requirements.yml
ansible-playbook build.yml
